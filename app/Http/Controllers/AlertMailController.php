<?php

namespace App\Http\Controllers;

use App\Mail\NotificationComment;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\User;
use App\Publication;
class AlertMailController extends Controller
{
     /**
     * GET @param email
     */
    public function sendEmail($publication_id){
        try{
            $informacion = "(data)";
            $user = Publication::find($publication_id)->user()->get();
            Mail::to($user[0]->email)->send(new NotificationComment($informacion));
            return response("email enviado",200);
        }catch(Exception $e){
            return response('Error ' + $e, 400);
        }

}


}
