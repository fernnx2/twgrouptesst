<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AlertMailController;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'publication_id'=>'required',
             'content' => 'required',
        ]);

        $maxComment = Comment::where([
            ['publication_id','=',$request->publication_id],
            ['user_id', '=', Auth::user()->id ]
        ])->count();

        if($maxComment < 1){
            try{
                $comment = new Comment;
                $comment->publication_id = $request->publication_id;
                $comment->user_id = Auth::user()->id;
                $comment->content = $request->content;
                $comment->status = "APROBADO";
                $comment->save();
                $send = new AlertMailController();
                $send->sendEmail($request->publication_id);
                return redirect()->route('publications.index')->with('success', 'Su comentario se ha enviado exitosamente');
            }
            catch(Exception $e){
            return redirect()->route('publications.index')->with('error', 'Comment error');
            }
        }
        else {
            return redirect()->route('publications.index')->with('error', 'No puedes volver a comentar la publicación');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
