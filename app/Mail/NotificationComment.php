<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationComment extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($information)
    {
        $this->data = $information;
    }

    public function build()
    {
        $address = 'devfernando95@gmail.com';
        $subject = 'Mensaje de alerta de comentario a su publicación!';
        $name = 'TWGroup';

        return $this->view('mails.confirmation')
                    ->from($address, $name)
                    ->cc($address, $name)
                    ->bcc($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([ 'message' => $this->data]);
    }
}

