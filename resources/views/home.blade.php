@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                 @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>

                <div class="card-body">
                    <h5>Funcionabilidades y requerimientos cumplidos</h5>
                    <ul>
                    <li>Registro y logueo de usuarios</li>
                    <li>Crear publicación</li>
                    <li>Ver comentarios de la publicación</li>
                    <li>Enviar un comentario</li>
                    <li>Validar cantidad de comentarios (1 por publicacion)</li>
                    <li>Enviar email al usuario dueño de la publicación</li>
                    <li>Utilización de bootstrap y blade</li>
                    </ul>
                </div>
                <a class="btn btn-primary" href="publications">VER PUBLICACIONES</a>
            </div>
        </div>
    </div>
</div>
@endsection
