@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
         <h1>Realizar Publicación</h1>
        </div>
    </div>

    <form action="{{ route('publications.store') }}" method="POST">
    @csrf
    <div class="container">
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            <strong>Título:</strong>
                <input class="form-control" name="title" placeholder="titulo"></input>
            </div>
            <div class="form-group">
                <strong>Content:</strong>
                <input class="form-control" style="height:150px" name="content" placeholder="content"></input>
            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Publicar</button>
        </div>
    </div>
    </div>
</form>
@endsection
