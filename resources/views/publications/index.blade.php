@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Publicaciones actuales</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('publications.create') }}"> Crear nueva publicación</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
     @if ($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>title</th>
            <th>Content</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($publications as $publication)
        <tr>
            <td>{{ $publication->id }}</td>
            <td>{{ $publication->title }}</td>
            <td>{{ $publication->content }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('publications.show',$publication->id) }}">Ver comentarios</a>
            </td>
        </tr>
        @endforeach
    </table>

    </div>
@endsection
