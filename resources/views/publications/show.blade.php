@extends('layouts.app')
@section('content')
    <div class = "container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Comentarios</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('publications.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <ul class="list-group list-group-flush">
        @foreach ($publication->comment as $comment)
            @if($comment->status == "APROBADO")
            <li class="list-group-item">{{$comment->content}}</li>
            @endif
       @endforeach
        </ul>
    </div>

    <form action="{{ route('comments.store') }}" method="POST">
    @csrf
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <input class="form-control" name="publication_id" hidden="true" value="{{ $publication->id }}"></input>
            </div>
            <div class="form-group">
                <strong>Comment:</strong>
                <textarea class="form-control" style="height:150px" name="content" placeholder="Comment"></textarea>
            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Comment</button>
        </div>
    </div>
</form>
</div>
@endsection
